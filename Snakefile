import os
import platform
import sys
import pandas as pd
from os.path import join as opj
from glob import glob
import itertools
from source.utils.parse_lists import get_genome_dict
from source.utils.parse_lists import get_sample_dict

# Read config file
configfile: "config.yaml"

# Check if multimapping reads should be counted
fc_params = "-F SAF -p -B"
if config["multimapping"]:
    fc_params += " -M"

uname = str(platform.uname())

# Set scratch path if running on Uppmax
if 'uppmax.uu.se' in uname:
    config["scratch"] = "$TMPDIR"
else:
    config["scratch"] = "temp"

# Find picard path
pythonpath = sys.executable
if not "picard_jar" in config.keys():
    envdir = '/'.join(pythonpath.split("/")[0:-2])
    for line in shell("find {envdir} -name picard.jar", iterable = True):
        basename = os.path.basename(line)
        if basename == "picard.jar":
            config["picard_jar"] = line
            config["picard_path"] = os.path.dirname(line)
else:
    config["picard_jar"] = os.path.expandvars(config["picard_jar"])
    config["picard_path"] = os.path.dirname(config["picard_jar"])

# Set constraints
wildcard_constraints:
    genome_name = "[A-Za-z0-9_\-\.]+"
wildcard_constraints:
    genome_id = "[A-Za-z0-9_\-\.]+"

# Parse genomes
genomes = get_genome_dict(config["genome_list"], config["min_completeness"], config["max_contamination"], config["min_genome_size"], config["max_genome_size"])
# Parse samples
if os.path.exists(config["sample_list"]):
    samples = get_sample_dict(config["sample_list"])
else:
    samples = {}
# Prokka suffixes
suffixes = ["err","faa","ffn","fna","fsa","gbk","gff","sqn","tbl","tsv","txt"]

# Include rules
include: "source/rules/prepare_dbCAN.rules"
include: "source/rules/prepare_tRNAs.rules"
include: "source/rules/prepare_eggnog.rules"
include: "source/rules/prepare_prokka.rules"
include: "source/rules/prepare_pfam.rules"
include: "source/rules/checkm.rules"
include: "source/rules/annotate_genomes.rules"
include: "source/rules/cluster_genomes.rules"
include: "source/rules/mapping.rules"
include: "source/rules/gtdb-tk.rules"

rule all:
    message: "Master workflow rule"
    input:
        collated_files = expand("results/collated/{genome_list}/{db}.tab",
            db = ["enzymes","pathways","modules","kos","pfams","dbCAN"],
            genome_list = os.path.basename(config["genome_list"])),
        abundance_files = expand("results/abundance/{genome_list}/{prefix}.tab",
            genome_list = os.path.basename(config["genome_list"]),
            prefix = ["tpm","tpm.percontig","raw_counts","raw_counts.percontig"]),
        cluster_file = opj("results","fastANI",os.path.basename(config["genome_list"]), "genome_clusters.tsv"),
        #map_files = expand(opj("results","annotation","{genome_id}","{genome_id}.cov"),
        #                   genome_id = genomes.keys())

rule preprocess:
    message: "Preprocessing genomes using CheckM"
    input:
        "results/checkm/{genome_list}/checkm_results.tab".format(genome_list = os.path.basename(config["genome_list"]))

rule gtdb_tk:
    message: "Running GTDB-TK on genomes"
    input:
        "results/gtdb/{genome_list}/gtdbtk.log".format(genome_list= os.path.basename(config["genome_list"]))

rule cluster_genomes:
    input: opj("results","fastANI",os.path.basename(config["genome_list"]), "genome_clusters.tsv")

rule annotate:
    message: "Generating genome annotation output"
    input:
        expand("results/collated/{genome_list}/{db}.tab",
            db = ["dbCAN","enzymes","pathways","modules","kos","pfams"], genome_list = os.path.basename(config["genome_list"]))

rule prokka:
    message: "Running prokka on genomes"
    input: expand("results/annotation/{genome_id}/prokka/{genome_id}.{suffix}", genome_id = genomes.keys(), suffix = suffixes)

rule eggnog:
    message: "Running eggnog-mapper on genomes"
    input: expand("results/annotation/{genome_id}/eggnog/{genome_id}.{db}.tab", genome_id = genomes.keys(), db = ["enzymes","pathways","modules","kos"])

rule pfam:
    message: "Running pfam_scan on genomes"
    input: expand("results/annotation/{genome_id}/pfam/{genome_id}.pfams.tab", genome_id = genomes.keys())

rule dbCAN:
    message: "Annotating genomes with dbCAN database"
    input:
        expand("results/annotation/{genome_id}/dbCAN/{genome_id}.dbCAN.parsed.tab", genome_id = genomes.keys())

rule map:
    message: "Mapping samples against genomes"
    input:
        expand(opj("results","map",os.path.basename(config["genome_list"]),"{sample}.fc.tab"),
            sample = samples.keys()),
        #expand(opj("results","annotation","{genome_id}","{genome_id}.cov"),genome_id = genomes.keys())

rule normalize:
    message: "Calculating normalized abundance of genomes in samples"
    input:
        expand("results/abundance/{genome_list}/{prefix}.tab",
            genome_list = os.path.basename(config["genome_list"]), prefix = ["tpm","tpm.percontig","raw_counts","raw_counts.percontig"])
