# NBIS-MAGS

## Overview

This repository is used to analyze individual microbial genomes
such as MAGs (**M**etagenome **A**ssembled **G**enomes) and SAGs
(**S**ingle **A**mplified **G**enomes).

## Installation
### Clone the repository

```
git clone https://bitbucket.org/scilifelab-lts/nbis-mags.git
cd nbis-mags
```

### Required software
Install the required conda environment for running snakemake.

```
mkdir envs/nbis-mags
conda env create -f envs/environment.yaml -p envs/nbis-mags
```

You can then add the path to your conda config to make it easier to
activate the environment.

```
conda config --add envs_dirs <full_path_to_git_repository>/nbis-mags/envs
```

Activate the installed environment.

```
conda activate nbis-mags
```


Some software is not included in the main environment. This means that
snakemake has to be run with the `--use-conda` flag in order to work
properly.

## Running the workflow

The workflow is designed to annotate genomes and map their abundance in
selected samples.

### Config file
Settings needed to run the workflow are found in [config.yaml](config.yaml)
and these can be changed depending on your needs.

### Running on Uppmax
To make use of the slurm queue system on Uppmax you can run the workflow
with the [cluster.yaml](cluster.yaml) file as:

```
snakemake -j 99 -k -p --cluster-config config/cluster.yaml \
--cluster "sbatch -A {cluster.account} -p {cluster.partition} \
-n {cluster.n}  -t {cluster.time} -e {cluster.log} -o {cluster.log} \
{cluster.extra}"
```


### Preprocessing steps
An initial analysis of genomes can be done using checkm. This gives
estimates of genome completeness and assigns a phylogeny. To run this
initial analysis run:

```
snakemake --use-conda -p preprocess
```

By default the workflow runs the analysis on genomes specified in
[genomes/genomes.tsv](genomes/genomes.tsv). However, you can specify another
list of genomes using the `--config genome_list=<your genome list>`
directive to snakemake, e.g.:

```
snakemake --use-conda --config genome_list=genomes/genomes.tsv -p preprocess
```

The genome list file needs to be in the following format:

| genome_id | name |
| ---- | ---- |
| genome1 | Some genome name1 |
| genome2 | Some genome name 2 |

The `genome_id` column contains a unique genome identifier while
the `name` column can specify a more descriptive name for the genome.

**Each genome must have a nucleotide fasta file with the suffix `.fna` in a
directory under data/genomes/<genome_id>.**

Output from checkm are placed under `results/checkm/<genome_list_name>/`.
This allows you to run preprocessing on several sets of genomes in parallel
and also means you can add genomes without having to rerun the checkm steps
for already analyzed genomes.

### Filtering genomes prior to analysis
Using the output from the preprocessing steps you can choose to run
the downstream analysis steps only on genomes meeting certain critera such as:
- minimum completeness
- maximum contamination
- minimum/maximum genome size

You can either specify the thresholds in the config file:

```yaml
min_completeness:
max_contamination:
min_genome_size:
max_genome_size:
```

or directly on the command line. To only annotate genomes with at least
80% completeness and at most 5% contamination you can run:

```
snakemake --config min_completeness=80 max_contamination=5 -p
```


### Functional annotation steps

Functional annotation is performed with:
- eggnog-mapper ([eggNOG](http://eggnogdb.embl.de/), [GO terms](http://www.geneontology.org/), [KEGG KO](http://www.genome.jp/kegg/ko.html), [metabolic reactions](http://bigg.ucsd.edu/))
- pfam_scan ([PFAM domains](https://pfam.xfam.org/))
- hmmsearch ([TIGRFAM domains](http://www.jcvi.org/cgi-bin/tigrfams/index.cgi))

### Clustering genomes by nucleotide similarity
The workflow includes steps to cluster SAGs and MAGs by nucleotide
identity using FastANI.

This allows you to group together genomes from different samples into
'genome clusters'. By default genomes with a maximum nucleotide
distance of 0.05 are clustered but this can be changed via
the `max_nuc_dist` config setting or via the command line:

```
snakemake --config max_nuc_dist=0.05 -p cluster_genomes
```

This produces a set of output files in the `results/fastANI/<genome-list>/` directory:

* genome_clusters.tsv: A table with genome cluster names and genome ids. The
clusters are sorted by number of genomes in the clusters.