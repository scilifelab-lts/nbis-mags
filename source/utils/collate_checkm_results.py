#!/usr/bin/env python

import pandas as pd
from argparse import ArgumentParser
import sys

def get_files(pat):
    import glob
    files = glob.glob(pat)
    return files


def concat_tables(files):
    df = pd.DataFrame()
    for f in files:
        _df = pd.read_table(f, index_col=0)
        df = pd.concat([df,_df])
    return df


def main():
    parser = ArgumentParser()
    parser.add_argument("--stats_pat", default="results/checkm/*/genome_stats.tab",
                        help="File path pattern for genome stats files. Defaults to 'results/checkm/*/genome_stats.tab'")
    parser.add_argument("--phylo_pat", default="results/checkm/*/phylogeny.tab",
                        help="File path pattern for genome stats files. Defaults to 'results/checkm/*/phylogeny.tab'")
    parser.add_argument("--genome_files", nargs="+", required=True,
                        help="Genome files to extract information from for good quality genomes")
    parser.add_argument("--out_file",
                        help="Write updated genome list to file. Defaults to stdout")

    args = parser.parse_args()
    header = []
    # Get genome stat files from glob
    stat_files = get_files(args.stats_pat)

    # Get phylogeny files
    phylo_files = get_files(args.phylo_pat)

    # Concatenate genome info files
    info_df = concat_tables(args.genome_files)
    header+=list(info_df.columns)
    # Concatenate stats files
    stat_df = concat_tables(stat_files)
    header+=list(stat_df.columns)
    # Concatenate phylogeny files
    phyl_df = concat_tables(phylo_files)
    header+=list(phyl_df.columns)
    # Merge tables
    df = info_df
    if len(stat_files) > 0:
        df = pd.merge(df, stat_df, left_index=True, right_index=True)
    if len(phylo_files) > 0:
        df = pd.merge(df, phyl_df, left_index=True, right_index=True)
    df = df.loc[:, header]
    df.index.name = "taxa"
    if not args.out_file:
        out = sys.stdout
    else:
        out = args.out_file
    df.to_csv(out, sep="\t")


if __name__ == '__main__':
    main()
