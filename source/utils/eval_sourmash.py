#!/usr/bin/env python

import pandas as pd
from Bio.SeqIO import index
from argparse import ArgumentParser

def main(args):
    df = pd.read_csv(args.classify_file, index_col=0)
    df = df.assign(contig=pd.Series([x.split(" ")[0] for x in df.index], index=df.index))
    df = df.assign(length=pd.Series([int(x.split("=")[-1]) for x in df.index], index=df.index))
    df.set_index("contig", inplace=True)

    num_found = df.loc[df.status=="found"]
    contig_frac_found = float(len(num_found)) / len(df)
    size_found = df.loc[df.status=="found","length"].sum()
    size_total = df["length"].sum()

    size_frac = float(size_found) / size_total
    print("{}/{} contigs classified ({}%)".format(len(num_found),len(df), round(contig_frac_found*100, 1)))
    print("{} bp classified ({}%)".format(size_found, round(size_frac*100, 1)))


    seqs_on_contigs_found = 0
    seqs = index(args.protfasta, "fasta")
    for item in seqs:
        contig = "_".join(item.split("_")[0:-1])
        if contig in num_found.index:
            seqs_on_contigs_found+=1
    print("{}/{} proteins on classified contigs ({}%)".format(seqs_on_contigs_found, len(seqs), round((float(seqs_on_contigs_found) / len(seqs))*100, 1)))




if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-c", "--classify_file", type=str,
                        help="Sourmash classification output")
    parser.add_argument("-p", "--protfasta", type=str,
                        help="Protein fasta file")

    args = parser.parse_args()
    main(args)