#!/usr/bin/env python

from argparse import ArgumentParser
import pandas as pd


def make_pairwise(fastani):
    unique_keys = []
    keymap = {}
    for i in fastani.index:
        keyset = set([fastani.loc[i,"Query"],fastani.loc[i,"Reference"]])
        key = str(keyset)
        if len(keyset) == 1:
            keymap[key] = {"G1": list(keyset)[0], "G2": list(keyset)[0]}
        else:
            keymap[key] = {"G1": list(keyset)[0], "G2": list(keyset)[1]}
        unique_keys.append(key)
    tmp = fastani.assign(key=pd.Series(unique_keys, index=fastani.index))
    _ = tmp.groupby("key").mean()[["ANI","Overlap"]].reset_index()
    l = [keymap[key] for key in _.key]
    pairwise = pd.merge(pd.DataFrame(l),_,left_index=True,right_index=True)
    pairwise.drop("key",axis=1,inplace=True)
    return pairwise


def read_fastani(f):
    fastani = pd.read_table(f, header=None, sep=" ", names=["Q","R","ANI","Overlapping","Total"])
    # Parse genome names from full file names
    fastani = fastani.assign(Query = pd.Series([x.split("/")[-1].rstrip(".fna") for x in fastani.Q], index=fastani.index))
    fastani = fastani.assign(Reference = pd.Series([x.split("/")[-1].rstrip(".fna") for x in fastani.R], index=fastani.index))
    fastani.drop(["Q","R"],axis=1,inplace=True)
    # Re-sort columns
    fastani = fastani.loc[:,["Query","Reference","ANI","Overlapping","Total"]]
    # Add column showing alignment overlap
    fastani = fastani.assign(Overlap=pd.Series(fastani.Overlapping / fastani.Total, index=fastani.index))
    pairwise = make_pairwise(fastani)
    return pairwise


def main(args):
    fastani = read_fastani(args.input)
    distmat = 1-pd.pivot_table(fastani.loc[fastani.Overlap>=args.overlap],index="G1",columns="G2",values="ANI").div(100)
    distmat.fillna(1, inplace=True)
    distmat.to_csv(args.output, sep="\t")


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", type=str, required=True,
                        help="Pairwise table results from FastANI")
    parser.add_argument("-o", "--output", type=str, required=True,
                        help="Distance matrix output")
    parser.add_argument("--overlap", type=float, default=0.75,
                        help="Minimum overlap between genomes in a pairwise comparison. Defaults to 0.75.")
    args = parser.parse_args()
    main(args)