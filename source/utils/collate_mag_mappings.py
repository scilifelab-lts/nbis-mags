#!/usr/bin/env python

import os
import sys
import pandas as pd
from glob import glob
from argparse import ArgumentParser
from Bio.SeqIO import parse
import logging

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
)


def map_contigs(binfiles):
    # Map bins to contigs
    contig_map = {}
    for f in binfiles:
        bin = os.path.basename(f).replace(".fasta",".2k")
        contig_map[bin] = []
        for record in parse(f, "fasta"):
            contig_map[bin].append(record.id)
    return contig_map


def read_mapfiles(mapfiles):
    sample_map = {}
    for f in mapfiles:
        sample = os.path.basename(f).replace("_pe.fc.tab", "")
        df = pd.read_table(f, header=0, comment="#", index_col=0)
        sample_map[sample] = df.copy()
    return sample_map


def write_mappings(bin_contig_map, sample_map, outdir):
    for bin, contigs in bin_contig_map.items():
        logging.info("Writing mappings for {} contigs in bin {}".format(len(contigs), bin))
        for sample, df in sample_map.items():
            os.makedirs(os.path.join(outdir, bin), exist_ok=True)
            outfile = os.path.join(outdir, bin, "{sample}.fc.tab".format(sample=sample))
            # Filter out to contigs belonging to the bin
            filtered = df.loc[contigs]
            filtered.rename(index = lambda x: "{}_{}".format(bin,x), inplace = True)
            filtered.to_csv(outfile, sep="\t")


def main(args):
    logging.info("Reading mapfiles from {mapdir}".format(mapdir=args.mapdir))
    mapfiles = glob(os.path.join(args.mapdir, "*.fc.tab"))
    logging.info("Reading binfiles from {bindir}".format(bindir=args.bindir))
    binfiles = glob(os.path.join(args.bindir, "*.fasta"))
    logging.info("Mapping bins to contigs")
    bin_contig_map = map_contigs(binfiles)
    logging.info("Storing map results")
    sample_map = read_mapfiles(mapfiles)
    write_mappings(bin_contig_map, sample_map, args.outdir)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-m", "--mapdir", type=str, required=True,
                        help="Map directory containing *.fc.tab files")
    parser.add_argument("-b", "--bindir", type=str, required=True,
                        help="Bin directory containig *.fasta files")
    parser.add_argument("-o", "--outdir", type=str, required=True,
                        help="Place *.fc.tab files in this outdir")
    args = parser.parse_args()
    main(args)