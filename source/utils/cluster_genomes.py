#!/usr/bin/env python

import networkx as nx
import pandas as pd
from argparse import ArgumentParser
import operator
import logging
import sys
import os
import numpy as np


logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
)


def read_dist_mat(f, fasta_names):
    dist_mat = pd.read_table(f, header=None, names=fasta_names)
    dist_mat.index = fasta_names
    return dist_mat


def read_fastani_distmat(f):
    genomes = list(pd.read_csv(f, index_col=0, skiprows=1, sep="\t", header=None, usecols=[0]).index)
    genomes = [os.path.basename(g).rstrip(".fna") for g in genomes]
    r = {}
    with open(f, 'r') as fh:
        for i, line in enumerate(fh):
            if i==0:
                continue
            line = line.rstrip()
            items = line.rsplit("\t")
            genome = os.path.basename(items[0]).rstrip(".fna")
            r[genome] = {}
            for j, item in enumerate(items[1:]):
                genome2 = genomes[j]
                if item == "NA":
                    item = np.nan
                else:
                    item = float(item)
                r[genome][genome2] = item
    df = pd.DataFrame(r)
    df.fillna(0, inplace=True)
    return 1-df.div(100)


def generate_linkage(dist_mat, max_dist):
    linkage = {}
    for i in range(len(dist_mat.index)):
        g1 = dist_mat.index[i]
        if not g1 in linkage.keys():
            linkage[g1] = {}
        for j in range(i+1,len(dist_mat.columns)):
            g2 = dist_mat.columns[j]
            if not g2 in linkage.keys():
                linkage[g2] = {}
            distance = dist_mat.iloc[i,j]
            if distance <= max_dist:
                linkage[g1][g2] = ""
                linkage[g2][g1] = ""
    return linkage


def cluster(linkage):
    g = nx.from_dict_of_dicts(linkage)
    clustered = []
    clusters = {}
    clust_num = 1
    for n in g.nodes():
        c = [n]
        if n in clustered: continue
        edges = list(nx.dfs_edges(g,n))
        for e in edges:
            n1,n2 = e
            clustered+=[n1,n2]
            c+=[n1,n2]
        c = list(set(c))
        clusters[clust_num] = c[:]
        clust_num+=1
    return clusters


def write_clusters(clusters, outfile):
    # Calculate cluster sizes
    cluster_sizes = {}
    for clust_num, l in clusters.items():
        cluster_sizes[clust_num] = len(l)
    # Sort clusters by sizes
    sorted_clusters = sorted(cluster_sizes.items(), key=operator.itemgetter(1), reverse=True)
    # Write table
    with open(outfile, 'w') as fh:
        for i, item in enumerate(sorted_clusters, start=1):
            old_num = item[0]
            for g in clusters[old_num]:
                fh.write("CLUSTER_{}\t{}\n".format(i, g))


def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", required=True,
                        help="Either distance matrix output from dnadiff_dist_matrix.py (default) or output table \
                             from fastANI")
    parser.add_argument("-o", "--outfile", required=True,
                        help="Cluster table outfile")
    parser.add_argument("-n", "--names", required=False, nargs="+",
                        help="Names of input fasta files.")
    parser.add_argument("-m", "--max_dist", default=0.05, type=float,
                        help="Maximum distance between genomes (defaults to 0.05).")
    parser.add_argument("--overlap", default=0.75, type=float,
                        help="Minimum fraction of overlap between genomes. Only applicable for fastANI input.")
    parser.add_argument("--fastANI", action="store_true",
                        help="Input is table of comparisons from fastANI")
    args = parser.parse_args()
    logging.info("Reading input and generating linkages.")
    if not args.fastANI:
        dist_matrix = read_dist_mat(args.input, args.names)
    else:
        dist_matrix = read_fastani_distmat(args.input)

    linkage = generate_linkage(dist_matrix, args.max_dist)
    logging.info("Generating clusters.")
    clusters = cluster(linkage)
    logging.info("{} clusters generated. Writing table of clusters to {}".format(len(clusters), args.outfile))
    write_clusters(clusters, args.outfile)
    
if __name__ == '__main__':
    main()
