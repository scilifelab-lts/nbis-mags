import pandas as pd, os

def get_genome_dict(f):
    genomes = {}
    df = pd.read_csv(f, sep="\t", index_col=0, header=0)
    for t in df.index:
        dir = df.loc[t,"dir"]
        name = df.loc[t,"name"].replace(" ","_")
        if os.path.exists(dir):
            genomes[str(t)] = {"dir": dir, "name": name}
    return genomes
