#!/usr/bin/env python

import pandas as pd
import os
import sys
from argparse import ArgumentParser
from Bio.SeqIO import parse

def main(args):
    df = pd.read_table(args.assemblyfile, index_col=0)
    for a in df["# assembly_accession"]:
        fna = df.loc[df["# assembly_accession"]==a,"fna"].values[0]
        taxid = df.loc[df["# assembly_accession"]==a,"taxid"].values[0]
        filepath = "{}/{}".format(downloaddir, os.path.basename(fna))
        if not os.path.exists(filepath):
            sys.stderr.write("Missing file for {}\n".format(a))
            continue
        for record in parse(gz.open(filepath, 'rt'), "fasta"):
            sys.stdout.write("{}\t{}\n".format(record.id, taxid))

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-a", "--assemblyfile", type=str,
                        help="Assembly file")
    parser.add_argument("-d", "--downloaddir", type=str,
                        help="Download directory")
    args = parser.parse_args()
    main(args)