#!/usr/bin/env python

from argparse import ArgumentParser
import pandas as pd
import os

def main(args):
    counts = {}
    for f in args.files:
        bin = os.path.basename(os.path.dirname(f))
        df = pd.read_table(f)
        sample = os.path.basename(df.columns[-1]).replace(args.sample_strip, "")
        if not sample in counts.keys():
            counts[sample] = {}
        if not bin in counts[sample].keys():
            counts[sample][bin] = 0
        s = df.sum()[-1]
        counts[sample][bin] = s
    df = pd.DataFrame(counts)
    df.to_csv(args.outfile, sep="\t")

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("files", type=str, nargs="+",
                        help="Count files")
    parser.add_argument("outfile", type=str,
                        help="Store mappings in this file")
    parser.add_argument("--sample_strip", type=str, default="_pe.markdup.bam",
                        help="String to strip from the sample in the featurecounts table")
    args = parser.parse_args()
    main(args)