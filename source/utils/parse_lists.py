import pandas as pd
import os


def get_taxonomy_dict(taxonomy):
    taxdict = {"kingdom": "", "phylum": "", "class": "", "order": "", "family": "", "genus": "", "species": ""}
    ranks = ["kingdom","phylum","class","order","family","genus","species"]
    rank_letters = {"k": "kingdom", "p": "phylum", "c": "class", "o": "order", "f": "family", "g": "genus", "s": "species"}
    try:
        taxonomy.split(";")
    except AttributeError:
        for rank in ranks:
            taxdict[rank] = "Unclassified"
        return taxdict
    for j, a in enumerate(taxonomy.split(";")):
        rank = a.split("_")[0]
        name = a.replace("{}__".format(rank),"").replace(" (root)","")
        taxdict[rank_letters[rank]] = name
    for rank in ranks[j+1:]:
        taxdict[rank] = "Unclassified.{}".format(name)
    for rank in ranks[j+1:]:
        taxdict[rank] = "Unclassified.{}".format(name)
    return taxdict


def get_sample_dict(f):
    samples = {}
    df = pd.read_csv(f, sep="\t", header=0, names=["sample","R1","R2"], index_col=0)
    for s in df.index:
        R1 = df.loc[s,"R1"]
        R2 = df.loc[s,"R2"]
        samples[s] = {"R1": R1, "R2": R2}
    return samples


def get_genome_dict(f, min_comp=False, max_cont=None, min_size=None, max_size=None):
    genomes = {}
    df = pd.read_csv(f, index_col=0, sep="\t")
    df = df.groupby(level=0).first()
    # Filter by genome stats if present
    if "Completeness" in df.columns and min_comp is not None:
        df = df.loc[df.Completeness >= min_comp]
    if "Contamination" in df.columns and max_cont is not None:
        df = df.loc[df.Contamination <= max_cont]
    if "Genome size (bp)" in df.columns:
        if min_size is not None:
            df = df.loc[df["Genome size (bp)"] >= min_size]
        if max_size is not None:
            df = df.loc[df["Genome size (bp)"] <= max_size]
    # Generate dictionary
    for t in df.index:
        if "Taxonomy" in df.columns:
            taxcol = "Taxonomy"
            tax = df.loc[t, taxcol]
            taxonomy = get_taxonomy_dict(tax)
        elif "Taxonomy (contained)" in df.columns:
            taxcol = "Taxonomy (contained)"
            tax = df.loc[t, taxcol]
            taxonomy = get_taxonomy_dict(tax)
        else:
            taxonomy = {"kingdom": "Unclassified", "phylum": "Unclassified", "class": "Unclassified",
                        "order": "Unclassified", "family": "Unclassified", "genus": "Unclassified",
                        "species": "Unclassified"}
        genomes[str(t)] = {"name": t, "taxonomy": taxonomy}
    return genomes
