import os, gzip as gz, pickle
import checkm
from argparse import ArgumentParser

def load(f):
    if ".gz" in f:
        with gz.open(f) as fh:
            pkl = pickle.load(fh)
    else:
        with open(f) as fh:
            pkl = pickle.load(fh)
    return pkl


def dump(output_pickle, outfile):
    if ".gz" in outfile:
        with gz.open(outfile, 'w') as fh_out:
            pickle.dump(output_pickle, fh_out)
    else:
        with open(outfile, 'w') as fh_out:
            pickle.dump(output_pickle, fh_out)


def add_pickles(input, output_pickle):
    for f in input:
        pkl = load(f)
        for key,value in pkl.items():
            output_pickle[key] = value
    return output_pickle


def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", nargs="+", required=True,
                        help="Input pickles")
    parser.add_argument("-o", "--output",
                        help="Output pickle")
    args = parser.parse_args()

    if os.path.exists(args.output):
        output_pickle = load(args.output)
        print("Loaded existing pickle {} with {} records".format(args.output,len(output_pickle)))
    else:
        output_pickle = {}

    output_pickle = add_pickles(args.input, output_pickle)

    dump(output_pickle, args.output)

if __name__ == '__main__':
    main()

