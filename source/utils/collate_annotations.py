#!/usr/bin/env python

import pandas as pd
from argparse import ArgumentParser
import os
import sys
import logging

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
)

def count_annotations(l):
    annots = []
    for item in l:
        try:
            annots+=item.split(",")
        except AttributeError:
            continue
    counts = {}
    for a in annots:
        a = a.lstrip().rstrip()
        try:
            counts[a] += 1
        except KeyError:
            counts[a] = 1
    return counts


def read_eggnog(files):
    logging.info("Reading EGGNOG annotations for {} genomes".format(len(files)))
    header = ["#query_name", "seed_eggNOG_ortholog", "seed_ortholog_evalue",
              "seed_ortholog_score", "predicted_gene_name",
              "GO_terms", "KEGG_KOs", "BiGG_reactions", "Annotation_tax_scope",
              "OGs", "bestOG|evalue|score", "COG cat", "eggNOG annot"]
    ko = pd.DataFrame()
    go = pd.DataFrame()
    cogcat = pd.DataFrame()
    for f in files:
        genome = os.path.basename(f).replace(".emapper.annotations", "")
        eggnog_df = pd.read_table(f, header=None, index_col=0, names=header)
        ko_counts = pd.DataFrame(count_annotations(eggnog_df.KEGG_KOs), index=[genome]).T
        ko = pd.concat([ko,ko_counts], axis=1)
        go_counts = pd.DataFrame(count_annotations(eggnog_df.GO_terms), index=[genome]).T
        go = pd.concat([go,go_counts], axis=1)
        cogcat_counts = pd.DataFrame(count_annotations(eggnog_df["COG cat"]), index=[genome]).T
        cogcat = pd.concat([cogcat,cogcat_counts], axis=1)
    return ko.fillna(0), go.fillna(0), cogcat.fillna(0)


def read_dbcan(files):
    logging.info("Reading dbCAN annotations for {} genomes".format(len(files)))
    dbcan = pd.DataFrame()
    for f in files:
        genome = os.path.basename(f).replace(".dbCAN.parsed.tab", "")
        dbcan_df = pd.read_table(f, header=None)
        dbcan_counts = pd.DataFrame(count_annotations(dbcan_df[0]), index=[genome]).T
        dbcan = pd.concat([dbcan, dbcan_counts], axis=1)
    return dbcan.fillna(0)


def main(args):
    filetype = (args.filetype).lower()
    if filetype == "eggnog":
        ko, go, cogcat = read_eggnog(args.files)
        ko.to_csv(os.path.join(args.out, "KEGG_KO.tsv"), sep="\t")
        go.to_csv(os.path.join(args.out, "GO_TERM.tsv"), sep="\t")
        cogcat.to_csv(os.path.join(args.out, "COGCAT.tsv"), sep="\t")
    elif filetype == "dbcan":
        dbcan = read_dbcan(args.files)
        dbcan.to_csv(os.path.join(args.out, "DBCAN.tsv"), sep="\t")


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("files", nargs="+",
                        help="Input files")
    parser.add_argument("out",
                        help="Output directory")
    parser.add_argument("--type", dest="filetype", type=str, default="eggnog",
                        help="Type of annotation (EggNOG, dbCAN)")
    args = parser.parse_args()
    main(args)