#!/usr/bin/env python

import pandas as pd
import sys
from Bio import SeqIO
from argparse import ArgumentParser


def get_contig_fasta(f):
    seq = SeqIO.index(f, "fasta")
    return seq


def get_genes_table(f):
    genes = pd.read_csv(f, header=None, names=["gene_id", "contig", "start", "stop", "strand"], index_col=1)
    return genes


def get_gene_seqs(seq, genes):
    gene_seqs = {}
    for contig in genes.index.unique():
        contig_seq = seq[contig]
        r = genes.loc[contig]
        if type(r) == pd.Series:
            r = pd.DataFrame(r).T
        for i in range(len(r)):
            gene_id, start, stop, strand = r.iloc[i]
            gene_seq = contig_seq[start:stop]
            if strand == -1:
                gene_seq = gene_seq.reverse_complement()
            gene_seqs[gene_id] = str(gene_seq.seq)
    return gene_seqs


def write_gene_seqs(gene_seqs):
    for key, seq in gene_seqs.items():
        sys.stdout.write(">{key}\n{seq}\n".format(key=key, seq=seq))


def main(args):
    seq = get_contig_fasta(args.contig_fasta)
    genes = get_genes_table(args.genes_file)
    gene_seqs = get_gene_seqs(seq, genes)
    write_gene_seqs(gene_seqs)


if __name__ == '__main__':
    parser = ArgumentParser(usage="python extract_gene_fasta.py genome_bin.fna genome_bin.genes > genome_bin.ffa")
    parser.add_argument("contig_fasta", type=str,
                        help="Contig fasta file")
    parser.add_argument("genes_file", type=str,
                        help="CSV file with gene_id, contig_id, start, stop, strand")
    args = parser.parse_args()
    main(args)
