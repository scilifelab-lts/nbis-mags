localrules: download_pfam, download_pfam_info, press_pfam

rule download_pfam:
    output:
        hmmfile=opj("resources","pfam","Pfam-A.hmm"),
        datfile=opj("resources","pfam","Pfam-A.hmm.dat"),
        versionfile=opj("resources","pfam","Pfam.version"),
    params:
        ftp = "ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release"
    shell:
        """
        curl -s -L -o {output.hmmfile}.gz {params.ftp}/Pfam-A.hmm.gz
        curl -s -L -o {output.datfile}.gz {params.ftp}/Pfam-A.hmm.dat.gz
        curl -s -L -o {output.versionfile}.gz {params.ftp}/Pfam.version.gz

        gunzip {output.hmmfile}.gz
        gunzip {output.datfile}.gz
        gunzip {output.versionfile}.gz
        """

rule download_pfam_info:
    output:
        clanfile=opj("resources","pfam_info","clan.txt"),
        info=opj("resources","pfam_info","Pfam-A.clans.tsv")
    params:
        ftp = "ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release"
    shell:
        """
        curl -s -L -o {output.clanfile}.gz {params.ftp}/database_files/clan.txt.gz
        curl -s -L -o {output.info}.gz {params.ftp}/Pfam-A.clans.tsv.gz

        gunzip {output.clanfile}.gz
        gunzip {output.info}.gz
        """

rule press_pfam:
    input:
        hmmfile=opj("resources","pfam","Pfam-A.hmm")
    output:
        expand(opj("resources","pfam","Pfam-A.hmm.h3{suffix}"), suffix = ["f","i","m","p"])
    conda: "../../envs/pfam_scan.yaml"
    shell:
        """
        hmmpress {input.hmmfile}
        """